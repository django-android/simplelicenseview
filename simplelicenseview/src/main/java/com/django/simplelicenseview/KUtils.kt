import android.os.Build

open class KUtils {

    companion object {
        private val kLicenseNames = arrayOf("MIT License",
                "Apache License 2.0",
                "GNU GPLv3",
                "GNU AGPLv3",
                "GNU LGPLv3",
                "The Unlicense",
                "Mozilla Public License 2.0")
        private val kLicenseLinks = arrayOf("https://choosealicense.com/licenses/mit/",
                "https://choosealicense.com/licenses/apache-2.0/",
                "https://choosealicense.com/licenses/gpl-3.0/",
                "https://choosealicense.com/licenses/agpl-3.0/",
                "https://choosealicense.com/licenses/lgpl-3.0/",
                "https://choosealicense.com/licenses/unlicense/",
                "https://choosealicense.com/licenses/mpl-2.0/")

        fun isSDK6(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
        }
        fun isSDK8(): Boolean {
            return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O
        }

        fun getLicense(index: Int): String {
            return kLicenseNames[index]
        }
        fun getLicenseLink(index: Int): String {
            return kLicenseLinks[index]
        }
    }
}