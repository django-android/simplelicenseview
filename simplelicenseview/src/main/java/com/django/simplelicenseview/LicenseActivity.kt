package com.django.simplelicenseview

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.view.animation.Animation
import android.view.animation.Animation.AnimationListener
import android.view.animation.TranslateAnimation
import android.widget.TextView


class LicenseActivity : AppCompatActivity() {

    private lateinit var rootView: View
    private lateinit var mToolbarText: TextView
    private lateinit var mRecyclerView: RecyclerView

    private var mUXDarkMode = false

    class Library(val name: String, val author: String, val licenseType: Int) {
        var notice: String? = null


        constructor(name: String, author: String, licenseType: Int, notice: String) : this(name, author, licenseType) {
            this.notice = notice
        }

        fun addNotice(notice: String): Library {
            return Library(name, author,licenseType, notice)
        }

        fun serialise(): String {
            return name + "," + author + "," + licenseType.toString() + "," + notice
        }
        companion object {
            const val INTENT_KEY = "libs"

            fun deserialise(data: String): Library {
                val stringBits = data.split(",")
                return Library(stringBits[0], stringBits[1], stringBits[2].toInt(), stringBits[3])
            }
            fun toSerial(libs: MutableList<Library>): String {
                val builder = StringBuilder()

                val i = libs.iterator()
                for (lib in i) {
                    builder.append(lib.serialise())
                    if(i.hasNext())
                        builder.append("|")
                }
                return builder.toString()
            }
        }
    }

    private var kLibraries = mutableListOf<Library>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_license)

        rootView = findViewById(R.id.rootView)
        mToolbarText = findViewById(R.id.fakeToolbar)
        mRecyclerView = findViewById(R.id.recyclerView)

        val dataArray = intent.getStringArrayExtra(Library.INTENT_KEY)
        var data = ""
        try {
            data = dataArray[0]
            mUXDarkMode = dataArray[1].toBoolean()
        }
        catch (e: Exception) {
            mUXDarkMode = false
        }
        initLibraries(data)
        setThemeMode()

        val adapter = LicenseAdapter(kLibraries, mUXDarkMode)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.adapter = adapter
    }

    private fun initLibraries(data: String) {
        if(data != "") {
            val libs = data.split("|")
            for (l in libs) {
                val lib = Library.deserialise(l)
                kLibraries.add(lib)
            }
        }

        //This must not be removed.
        val kLibThisLibrary = Library("Simple License View", "Django Cass", 1).addNotice("Copyright (c) 2018 Django Cass")

        kLibraries.add(kLibThisLibrary)
    }

    private fun setThemeMode() {
        setViewColourMode()
        setUXOptions()
    }

    private fun setViewColourMode() {
        val mColourNightBasic = resources.getColor(R.color.colourNightBasic)
        val mColourDayBasic = resources.getColor(R.color.colourDayBasic)

        val mColourTextPrimaryNight = resources.getColor(android.R.color.primary_text_dark)
        val mColourTextPrimaryDay = resources.getColor(android.R.color.primary_text_light)

        rootView.setBackgroundColor(if (mUXDarkMode) mColourNightBasic else mColourDayBasic)
        mToolbarText.setTextColor(if (mUXDarkMode) mColourTextPrimaryNight else mColourTextPrimaryDay)
    }

    @SuppressLint("InlinedApi")
    private fun setUXOptions() {
        if (!KUtils.isSDK6()) {
            return
        }
        val colorDark = getColor(R.color.colourNightBasic)
        val colorLight = getColor(R.color.colourDayBasic)
        val window = window
        var flags = rootView.systemUiVisibility
        flags = if (mUXDarkMode)
            flags.and(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv())
        else
            flags.or(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        if (KUtils.isSDK8()) {
            flags = if (mUXDarkMode)
                flags.and(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR.inv())
            else
                flags.or(View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR)
            window.navigationBarColor = if (mUXDarkMode) colorDark else colorLight
        }
        rootView.systemUiVisibility = flags
        window.statusBarColor = if (mUXDarkMode) colorDark else colorLight
    }
}
class LicenseAdapter(private val kLibraries: MutableList<LicenseActivity.Library>, private val kUXDark: Boolean) : RecyclerView.Adapter<LicenseAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_item_license, parent, false))
    }

    override fun getItemCount(): Int {
        return kLibraries.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(kLibraries[position], kUXDark)
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(library: LicenseActivity.Library, kUXDark: Boolean) {
            val txtName = itemView.findViewById<TextView>(R.id.txtName)
            val txtInfo = itemView.findViewById<TextView>(R.id.txtInfo)

            txtName.text = library.name
            if(library.notice != "")
                txtInfo.text = itemView.context.getString(R.string.ux_license_info, library.author, KUtils.getLicense(library.licenseType), library.notice, KUtils.getLicenseLink(library.licenseType))
            else
                txtInfo.text = itemView.context.getString(R.string.ux_license_info_no_notice, library.author, KUtils.getLicense(library.licenseType), KUtils.getLicenseLink(library.licenseType))
//            txtName.setOnClickListener({
//                if(txtInfo.visibility == View.VISIBLE)
//                    txtInfo.visibility = View.GONE
//                else
//                    txtInfo.visibility = View.VISIBLE
//            })
            val collapseAnim = TranslateAnimation(0.0f, 0.0f, 0.0f, -txtInfo.height.toFloat())
            collapseAnim.setAnimationListener(object : AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationStart(p0: Animation?) {

                }

                override fun onAnimationEnd(animation: Animation) {
                    txtInfo.visibility = View.GONE
                }
            })
            collapseAnim.duration = 300
            collapseAnim.interpolator = AccelerateInterpolator(0.5f)
            val openAnim = TranslateAnimation(0.0f, 0.0f, 0.0f, -txtInfo.height.toFloat())
            openAnim.setAnimationListener(object : AnimationListener {
                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationStart(p0: Animation?) {
                    txtInfo.visibility = View.VISIBLE
                }

                override fun onAnimationEnd(animation: Animation) {

                }
            })
            openAnim.duration = 300
            openAnim.interpolator = AccelerateInterpolator(0.5f)
            txtName.setOnClickListener {
                if(txtInfo.visibility == View.VISIBLE)
                    txtInfo.startAnimation(collapseAnim)
                else
                    txtInfo.startAnimation(openAnim)
            }
            //Set colours
            if(kUXDark) {
                txtName.setTextColor(itemView.context.resources.getColor(android.R.color.primary_text_dark))
                txtInfo.setTextColor(itemView.context.resources.getColor(android.R.color.secondary_text_dark))
            }
            else {
                txtName.setTextColor(itemView.context.resources.getColor(android.R.color.primary_text_light))
                txtInfo.setTextColor(itemView.context.resources.getColor(android.R.color.secondary_text_light))
            }
        }
    }

}
